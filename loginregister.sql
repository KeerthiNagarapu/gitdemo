create database kpdemo;
use kpdemo;
use user;

INSERT INTO mysql.user (Host, User, Password) VALUES ('%', 'root', password('root'));
GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION;

SELECT user,authentication_string,plugin,host FROM mysql.user;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
FLUSH PRIVILEGES;

show tables;
select * from amazonuser;
select * from Register_Entity;
select * from address;
select * from orders;
select * from employee;
select * from products;

insert into products (product_id, description,product) values ('1234', 'for Business','Skype');
insert into products (product_id, description,product) values ('1244', 'Teams','Microsoft');
insert into register (id, password,user) values (1, 'panda','kheer');
insert into amazonuser (id, password,user_name) values (null, 'pandas','kheers');
insert into amazonuser (id, password,user_name) values (null, 'pandau','kheeru');

select * from products;